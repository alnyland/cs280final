#include "bst.h"
#include <iostream>
using namespace std;

void bst::destroy(node* n) {
    if (n!= nullptr) {
        destroy(n->leftChild);
        destroy(n->rightChild);
        delete n;
        n = nullptr;
    }
}

bst::bst() {
    root = nullptr;    
}

bst::~bst() {
    destroy(root);
}

void bst::add(double distance) {
    node* tmp = new node();
    tmp->distance = distance;
    tmp->leftChild = nullptr;
    tmp->rightChild = nullptr;
    tmp->parent = nullptr;
    if (root == nullptr) {
        root = tmp;
    } else {
        node* runner = root;
        node* parent = runner;
        while (runner != nullptr) {
            parent = runner;
            if (distance < runner->distance) {
                runner = runner->leftChild;
            } else {
                runner = runner->rightChild;
            }
        }

        if (distance < parent->distance) {
            parent->leftChild = tmp;
        } else {
            parent->rightChild = tmp;
        }
        tmp->parent = parent;
    }
}

void bst::print() {
    print(root);
}
void bst::print(node* tmp) {
    if (tmp != nullptr) {
        print(tmp->leftChild);
        cout << tmp->distance << endl;
        print(tmp->rightChild);
    }
}

void bst::printN(int n) {}
