#pragma once
#include "types.h"

class graph {
    int size;
    point randomPoint();
public:
    graph(int, int);
    ~graph();
    void addVertex(point);
    void delVertex(point);
    int fill(int);
    point getRandomVertex();
    void printBFT(point);
    void printDFT(point);
    double dist(point, point);
};
