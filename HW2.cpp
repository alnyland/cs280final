#include <iostream>
#include <fstream>
#include <typeinfo>
#include <sstream>
using namespace std;

int numDoubles = 0;
int numDuplicates = 0;

struct wordItem {
    string word;
    int count;
};

void doubleArraySize(wordItem *&wordItemList, int &length) {
    length *= 2;
    wordItem* newWordItemList = new wordItem[length];
    for (int i = 0; i < length / 2; i++) {
        newWordItemList[i] = wordItemList[i];
    }
    delete [] wordItemList;
    wordItemList = newWordItemList;
    numDoubles++;
}

void addWord(wordItem *&wordItemList, int &wordIndex, int &length, string nextWord) {
    bool found = false;
    for (int i = 0; i < wordIndex && !found; i++) {
        if (nextWord == wordItemList[i].word) {
            wordItemList[i].count++;
            numDuplicates++;
            found = true;
        }
    }
    
    if (!found) {
        if (wordIndex == length) {
            doubleArraySize(wordItemList, length);
        }
        wordItemList[wordIndex].word = nextWord;
        wordItemList[wordIndex].count = 1;
        wordIndex++;
    }
}

void arraySort(wordItem wordItemList[], int wordIndex) {
    for (int i = 0; i < wordIndex - 1; i++) {
        for (int j = 0; j < wordIndex - i - 1; j++) {
            if (wordItemList[j].count < wordItemList[j+1].count) {
                wordItem temp = wordItemList[j];
                wordItemList[j] = wordItemList[j+1];
                wordItemList[j+1] = temp;
            }
        }
    }
}

void printTopN(wordItem wordItemList[], int topN) {
    for (int i = 0; i < topN; i++) {
        cout << wordItemList[i].word << ", " << wordItemList[i].count << endl;
    }
}

void printArr(wordItem wordItemList[], int wordIndex) {
    for(int i = 0; i < wordIndex; i++) {
        cout << wordItemList[i].word << ", " << wordItemList[i].count << endl;
    }
}

int main(int argc, const char * argv[]) {
    if (argc < 2) {
        cout << "Please specify a filename." << endl;
        exit(0);
    }
    
    int wordIndex = 0;
    int length = 1;
    wordItem* wordItemList = new wordItem[length];

    int topN = 4;
    
    ifstream in_f(argv[1]);
    string currentLine;
    
    while (getline(in_f, currentLine)) {
        stringstream ss(currentLine);
        string word;
        while (ss >> word) {
            addWord(wordItemList, wordIndex, length, word);
        }
    }

    arraySort(wordItemList, wordIndex);
    printTopN(wordItemList, topN);
    cout << "Array doubles: " << numDoubles << endl;
    cout << "Total number of unique words: " << wordIndex << endl;
    cout << "Total number of duplicate words: " << numDuplicates << endl;
    
    delete [] wordItemList;
    return 0;
}

