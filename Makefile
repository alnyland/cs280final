run: main.cpp types.h bst.o graph.o sort.o
	g++ -o run main.cpp bst.o graph.o sort.o
	make clean
bst.o: bst.h bst.cpp
	g++ -c bst.cpp
graph.o: graph.h graph.cpp
	g++ -c graph.cpp
sort.o: sort.h sort.cpp
	g++ -c sort.cpp
clean:
	rm *.o
