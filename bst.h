#pragma once

typedef struct node {
    double distance;
    node* leftChild;
    node* rightChild;
    node* parent;
} node;

class bst {
    node* root;
    void destroy(node* n);
public:
    bst();
    ~bst();
    void add(double distance);
    void print();
    void print(node*);
    void printN(int n);
};
