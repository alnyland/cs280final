#include <iostream>
#include "bst.h"
#include <vector>
#include <chrono>
#include <algorithm>

using namespace std;
//using namespace std::chrono;
#include <time.h>
#include <stdlib.h>

void mergeSort(vector<int> &, int, int);
void merge(vector<int> &, int, int, int);

unsigned int gseed = 2<<3;
inline int frand() {
    gseed = (214013)*gseed+2531011;
    return (gseed>>16) & 0x7FFF;
}
    

struct timed {
    clock_t fillA, fillT, sort, generate, total;
};

int main(int argc, char** argv) {
    int nPoints = 2<<5;
    int nIter   = 2; //2<<4;
    int step = 2, mult = step;
    if (argc > 1) {
        nPoints = stoi(argv[1]);
    }

    //metadata
    cout << "Amount of points: " << nPoints << endl;
    cout << "Amount of iterations: " << nIter << endl;

    srand(time(NULL));
    gseed = time(NULL);

    timed times[nIter];
    int sizes[nIter];

    for (int i = 0; i < nIter; i++) {
        clock_t totalStart = clock();
        clock_t part1s = clock();
        //auto tStart = chrono::high_resolution_clock::now();

        //Generating data structures from genPoints
        cout << "\tGenerating" << endl;
        vector<int> genPoints;
        clock_t generateStart = clock();
        for (int j = 0; j < nPoints; j++) {
            int r = rand() % 10 + 1;
            r = rand();
            //cout << "--- " << r << endl;
            genPoints.push_back(r);
            //genPoints.push_back(nPoints-j);
        }
        clock_t generateEnd = clock();

        //Generating vector from data points
        cout << "\tFilling vector" << endl;
        vector<int> arr;
        clock_t fillAStart = clock();
        for (int j = 0; j < genPoints.size(); j++) {
            arr.push_back(genPoints[j]);
        }
        clock_t fillAEnd = clock();

        clock_t part1e = clock();
        clock_t part2s = clock();
        //Generating tree from data points
        cout << "\tFilling tree" << endl;
        clock_t fillTStart = clock();
        bst tree;
        for (int j = 0; j < genPoints.size(); j++) {
            tree.add(genPoints.at(j));
        }
        clock_t fillTEnd = clock();

        cout << "\tSorting vector" << endl;
        clock_t sortStart = clock();
        //mergeSort(array);
        sort(arr.begin(), arr.end());
        clock_t sortEnd = clock();

        //Printing
        cout << "\tPrinting" << endl;
        tree.print();
        //a bunch of timing code
        clock_t part2e = clock();
        clock_t totalEnd = clock();
        //auto tEnd = chrono::high_resolution_clock::now();

        times[i].fillA = fillAEnd - fillAStart;
        times[i].fillT = fillTEnd - fillTStart;
        times[i].sort = sortEnd - sortStart;
        times[i].generate = generateEnd - generateStart;
        times[i].total = totalEnd - totalStart;
        //cout << "Time: " << chrono::duration_cast<chrono::microseconds>(tEnd - tStart).count() << endl;
    }

    //print output
    cout << "----------------------" << endl;
    cout << "ID, " << "Generate, " << "FillArray, ";
    cout << "FillTree, " << "Sort, " << "Total" << endl;
    for (int i = 0; i < nIter; i++) {
        //double time_taken = (double) (end - start) / (double) CLOCKS_PER_SEC;
        //print in format 
        double generateTime = (double) times[i].generate / (double) CLOCKS_PER_SEC,
               fillATime    = (double) times[i].fillA / (double) CLOCKS_PER_SEC,
               fillTTime    = (double) times[i].fillT / (double) CLOCKS_PER_SEC,
               sortTime     = (double) times[i].sort / (double) CLOCKS_PER_SEC,
               totalTime    = (double) times[i].total / (double) CLOCKS_PER_SEC;
        cout << i << ",";
        cout << generateTime << ",";
        cout << fillATime << ",";
        cout << fillTTime << ",";
        cout << sortTime << ",";
        cout << totalTime << endl;
    }
    //srand(time(NULL));
    vector<int> randoms;
    for (int i = 0; i < 10; i++) {
        randoms.push_back(rand() % 10 + 1);
    }
    mergeSort(randoms, 0, randoms.size());
    for (int i : randoms) {
        cout << i << " ";
    }
    cout << endl;
}

void mergeSort(vector<int> &distances, int start, int end) {
    if (start < end) {
        int mid = (start + end) / 2;
        mergeSort(distances, start, mid);
        mergeSort(distances, mid+1, end);
        merge(distances, start, mid, end);
    }
}

void merge(vector<int> &distances, int start, int mid, int end) {
    int temp[end - start + 1];
    int i = start;
    int j = mid + 1;
    int k = 0;

    while (i <= mid && j <= end) {
        if (distances[i] <= distances[j]) {
            temp[k] = distances[i];
            k += 1;
            i += 1;
        } else {
            temp[k] = distances[j];
            k += 1;
            j += 1;
        }
    }

    while (i <= mid) {
        temp[k] = distances[i];
        k += 1;
        i += 1;
    }

    while (j <= end) {
        temp[k] = distances[j];
        k += 1;
        j += 1;
    }

    for (i = start; i <= end; i++) {
        distances[i] = temp[i - start];
    }
}
